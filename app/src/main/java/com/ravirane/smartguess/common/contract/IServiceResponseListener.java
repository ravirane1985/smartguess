package com.ravirane.smartguess.common.contract;

/**
 * Created by ravirane on 1/23/16.
 */
public interface IServiceResponseListener {

    public void onSuccess(IResponseAction responseAction);
    public void onFailure(IResponseAction responseAction);

}

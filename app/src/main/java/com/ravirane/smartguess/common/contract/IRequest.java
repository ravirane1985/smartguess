package com.ravirane.smartguess.common.contract;

import java.util.Map;

/**
 * Created by ravirane on 1/23/16.
 */
public interface IRequest {

    public enum method {
        GET,
        POST,
        DELETE,
        PUT
    }

    public enum action {
        GET_GAMES,
        GET_GAME_QUESTIONS,
        GET_GAME_HINTS
    }


    method getRequestMethod();
    action getRequestAction();
    String getURLContext();
    Map getReqParamMap();

}

package com.ravirane.smartguess.common.impl;

import com.ravirane.smartguess.common.contract.IRequest;
import com.ravirane.smartguess.common.contract.IServiceResponseListener;

/**
 * Created by ravirane on 1/23/16.
 */
public class ServiceHandler {

    IServiceResponseListener callback;

    private static ServiceHandler serviceHandler;

    public static ServiceHandler getInstance() {
      //  if(serviceHandler == null) {
       //     serviceHandler = new ServiceHandler();
       // }
            return new ServiceHandler();
    }

    public void processRequest(IRequest request, IServiceResponseListener callback) {
         this.callback = callback;
         new NpmService(this).execute(request);
    }

}

package com.ravirane.smartguess.common.requests;

import com.ravirane.smartguess.common.contract.IRequest;

import java.util.Map;

/**
 * Created by ravirane on 1/23/16.
 */
public class GameGuessRequest implements IRequest {

    private method requestType = method.GET;

    private action requestAction = action.GET_GAMES;

    public Map<String,String> reqParamMap;

    public GameGuessRequest(method requestType, action requestAction, Map paramMap) {
        this.requestType = requestType;
        this.requestAction = requestAction;
        this.reqParamMap = paramMap;
    }

    @Override
    public method getRequestMethod() {
        return requestType;
    }

    @Override
    public action getRequestAction() {
        return requestAction;
    }

    @Override
    public String getURLContext() {

        if( this.requestType == method.GET && this.requestAction == action.GET_GAMES ) {
            return "/game";
        } else if( this.requestType == method.GET && this.requestAction == action.GET_GAME_QUESTIONS ) {
            return "/gamequestions";
        } else {
            return null;
        }
    }

    @Override
    public Map getReqParamMap() {
        return reqParamMap;
    }
}

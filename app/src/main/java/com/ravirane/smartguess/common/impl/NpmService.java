package com.ravirane.smartguess.common.impl;

import android.os.AsyncTask;
import android.util.Log;

import com.ravirane.smartguess.backendlessModels.BmGameHints;
import com.ravirane.smartguess.backendlessModels.BmGameQuestion;
import com.ravirane.smartguess.backendlessModels.BmGames;
import com.ravirane.smartguess.common.contract.IRequest;
import com.ravirane.smartguess.model.Game;
import com.ravirane.smartguess.model.GameHint;
import com.ravirane.smartguess.model.GameQuestion;
import com.ravirane.smartguess.service.IApiMethods;
import com.ravirane.smartguess.util.ServiceURL;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

/**
 * Created by ravirane on 1/23/16.
 */
public class NpmService extends AsyncTask<IRequest, Void, Void> {

    ServiceHandler serviceHandler;

    Retrofit retrofit;

    IRequest request;

    public NpmService(ServiceHandler serviceHandler) {
        this.serviceHandler = serviceHandler;
    }

    @Override
    protected void onPreExecute() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
                                      @Override
                                      public Response intercept(Interceptor.Chain chain) throws IOException {
                                          Request original = chain.request();

                                          Request request = original.newBuilder()
                                                  .header("application-id", "E2048415-E78D-A595-FF2A-A3C0A0234800")
                                                  .header("secret-key",  "AC243D34-C02C-221C-FFC9-4BC8023BAA00")
                                                  .method(original.method(), original.body())
                                                  .build();

                                          return chain.proceed(request);
                                      }
                                  });

        OkHttpClient client = httpClient.build();

        retrofit = new Retrofit.Builder().baseUrl(ServiceURL.getBaseURL())
                .addConverterFactory(GsonConverterFactory.create()).client(client).build();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

    @Override
    protected Void doInBackground(IRequest... requests) {

        request = requests[0];
        ResponseActionImpl responseAction = new ResponseActionImpl();
        try {
            if(request.getRequestAction() == IRequest.action.GET_GAMES &&
                    request.getRequestMethod() == IRequest.method.GET) {
                IApiMethods iApiMethods = retrofit.create(IApiMethods.class);
                List<Game> games = getGames(iApiMethods);
                responseAction.responseActionMap.put("games", games);
            } else if(request.getRequestAction() == IRequest.action.GET_GAME_QUESTIONS &&
                    request.getRequestMethod() == IRequest.method.GET) {
                Map<String,String> paramMap = request.getReqParamMap();
                String whrParam = paramMap.get("where");
                IApiMethods iApiMethods = retrofit.create(IApiMethods.class);
                List<GameQuestion> gameQuestions = getGamesQuestions(iApiMethods, whrParam);
                responseAction.responseActionMap.put("gameQuestion", gameQuestions);
            } else if(request.getRequestAction() == IRequest.action.GET_GAME_HINTS &&
                    request.getRequestMethod() == IRequest.method.GET) {
                Map<String,String> paramMap = request.getReqParamMap();
                String whrParam = paramMap.get("where");
                IApiMethods iApiMethods = retrofit.create(IApiMethods.class);
                List<GameHint> gameHints = getGameHints(iApiMethods, whrParam);
                responseAction.responseActionMap.put("gameHints", gameHints);
            }
            Log.e("", "doInBack : " + serviceHandler.callback.toString() + " - " + request.getRequestAction().toString());
            serviceHandler.callback.onSuccess(responseAction);
        } catch (Exception e) {
             e.printStackTrace();
        }
        return null;
    }

    private List<GameHint> getGameHints(IApiMethods iApiMethods, String whrParam)  throws Exception{
        Call<BmGameHints> call = iApiMethods.getGameHints(whrParam);
        BmGameHints bmGameHints = null;
        bmGameHints = call.execute().body();
        return bmGameHints.data;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    public List<Game> getGames(IApiMethods iApiMethods) throws Exception{

        Call<BmGames> call = iApiMethods.getGames();
        BmGames bMgames = null;
        bMgames = call.execute().body();
        return bMgames.data;
    }

    public List<GameQuestion> getGamesQuestions(IApiMethods iApiMethods, String whrParam) throws Exception{
        Call<BmGameQuestion> call = iApiMethods.getGameQuestions(whrParam);
        BmGameQuestion bmGameQuestion = null;
        bmGameQuestion = call.execute().body();
        return bmGameQuestion.data;
    }



}

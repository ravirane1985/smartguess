package com.ravirane.smartguess.service;

import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.ravirane.smartguess.common.contract.IResponseAction;
import com.ravirane.smartguess.common.contract.IServiceResponseListener;
import com.ravirane.smartguess.common.impl.ResponseActionImpl;
import com.ravirane.smartguess.common.impl.ServiceHandler;
import com.ravirane.smartguess.common.requests.GameGuessRequest;
import com.ravirane.smartguess.dao.GameHintDao;
import com.ravirane.smartguess.dao.GameQuestionDao;
import com.ravirane.smartguess.dao.UserGameDao;
import com.ravirane.smartguess.dao.UserQuestionDao;
import com.ravirane.smartguess.model.GameHint;
import com.ravirane.smartguess.model.GameQuestion;
import com.ravirane.smartguess.model.UserGame;
import com.ravirane.smartguess.model.UserQuestion;
import com.ravirane.smartguess.screens.GameSelectorActivity;
import com.ravirane.smartguess.util.DatabaseHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ravirane on 2/21/16.
 */
public class GameQuestionLoader implements IServiceResponseListener {

    int gameId;
    ImageView imgGameStatus;
    ProgressBar pb;
    GameQuestionLoader gameQuestionLoader;
    GameSelectorActivity gameSelectorActivity;
    ArrayList<GameQuestion> gameQuestions;
    ArrayList<GameHint> gameHints;

    public GameQuestionLoader(int gameId, ImageView imgGameStatus, ProgressBar pb,GameSelectorActivity gameSelectorActivity) {
        this.imgGameStatus = imgGameStatus;
        this.gameId = gameId;
        this.pb = pb;
        this.gameSelectorActivity = gameSelectorActivity;
    }

    public void loadGame() {
        gameQuestionLoader = this;
        new Thread(new Runnable() {
            @Override
            public void run() {
                Map<String,String> paramMap = new HashMap<>();
                String val = "gameId=" + gameId;
                paramMap.put("where",val);
                GameGuessRequest request = new GameGuessRequest(GameGuessRequest.method.GET,
                        GameGuessRequest.action.GET_GAME_QUESTIONS,paramMap);
                ServiceHandler.getInstance().processRequest(request, gameQuestionLoader);
                Log.e("", "load Game : " + gameQuestionLoader.toString() + " - " + gameId);
            }
        }).start();
    }

    @Override
    public void onFailure(IResponseAction responseAction) {

    }

    @Override
    public void onSuccess(final IResponseAction responseAction) {
        if(gameQuestions == null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Object obj = ((ResponseActionImpl) responseAction).responseActionMap.get("gameQuestion");
                    gameQuestions = (ArrayList<GameQuestion>) obj;

                    GameQuestionDao gameQuestionDao = new GameQuestionDao();
                    UserGame userGame = new UserGame();
                    userGame.gameId = gameId;
                    userGame.isEnabled = 1;
                    UserGameDao userGameDao = new UserGameDao();
                    List<UserQuestion> userQuestions = getUserQuestions(gameQuestions);
                    UserQuestionDao userQuestionDao = new UserQuestionDao();

                    DatabaseHelper.openDb();
                    gameQuestionDao.saveGameQuestions(gameQuestions);
                    userGameDao.upsertUserGame(userGame);
                    userQuestionDao.saveUserQuestions(userQuestions);
                    DatabaseHelper.closeDb();

                    Map<String,String> paramMap = new HashMap<>();
                    String val = "gameId=" + gameId;
                    paramMap.put("where",val);
                    GameGuessRequest request = new GameGuessRequest(GameGuessRequest.method.GET,
                            GameGuessRequest.action.GET_GAME_HINTS,paramMap);
                    ServiceHandler.getInstance().processRequest(request, gameQuestionLoader);
                }
            }).start();
        } else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Object obj = ((ResponseActionImpl) responseAction).responseActionMap.get("gameHints");
                    gameHints = (ArrayList<GameHint>) obj;

                    GameHintDao gameHintDao = new GameHintDao();
                    DatabaseHelper.openDb();
                    gameHintDao.saveGameHints(gameHints);
                    DatabaseHelper.closeDb();

                    gameSelectorActivity.updateGameQuestionLoadSuccess(imgGameStatus, pb);
                }
            }).start();
        }
    }

    private List<UserQuestion> getUserQuestions(ArrayList<GameQuestion> gameQuestions) {
        List<UserQuestion> userQuestions = new ArrayList<>();
        if(gameQuestions != null) {
            for(GameQuestion gameQuestion : gameQuestions) {
                UserQuestion userQuestion = new UserQuestion();
                userQuestion.gameId = gameQuestion.gameId;
                userQuestion.questionId = gameQuestion.questionId;
                userQuestion.currentHintId = 999;
                userQuestion.isAnsweredCorrectly = 0;
                userQuestions.add(userQuestion);
            }
        }
        return userQuestions;
    }
}

package com.ravirane.smartguess.service;

import com.ravirane.smartguess.backendlessModels.BmGameHints;
import com.ravirane.smartguess.backendlessModels.BmGameQuestion;
import com.ravirane.smartguess.backendlessModels.BmGames;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by ravirane on 1/13/16.
 */
public interface IApiMethods {

    @GET("/v1/data/Games")
    Call<BmGames> getGames();

    @GET("/v1/data/GameQuestion")
    Call<BmGameQuestion> getGameQuestions(@Query("where") String whereParam);

    @GET("/v1/data/GameHints")
    Call<BmGameHints> getGameHints(@Query("where") String whereParam);
}

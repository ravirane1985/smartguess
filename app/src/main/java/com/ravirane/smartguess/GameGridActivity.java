package com.ravirane.smartguess;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.ravirane.smartguess.adapter.GameGridSelectorAdapter;
import com.ravirane.smartguess.model.GameQuestion;

import java.util.ArrayList;


public class GameGridActivity extends AppCompatActivity {

    private GridLayoutManager gridLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_grid);
        Intent intent = getIntent();
        ArrayList<GameQuestion> gameQuestions =  (ArrayList<GameQuestion>) intent.getSerializableExtra("gameQuestions");
        gridLayoutManager = new GridLayoutManager(GameGridActivity.this, 3);

        RecyclerView rView = (RecyclerView)findViewById(R.id.recycler_view_game_grid);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(gridLayoutManager);

        GameGridSelectorAdapter gameGridSelectorAdapter = new GameGridSelectorAdapter(gameQuestions);
        rView.setAdapter(gameGridSelectorAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game_grid, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

package com.ravirane.smartguess.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.ravirane.smartguess.R;

/**
 * Created by ravirane on 1/26/16.
 */
public class FullPageWaitActivity extends RelativeLayout {

    public FullPageWaitActivity(Context context) {
        super(context);
    }

    public FullPageWaitActivity(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FullPageWaitActivity(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void loadView() {
       inflate(getContext(), R.layout.layout_loader, this);
    }

}

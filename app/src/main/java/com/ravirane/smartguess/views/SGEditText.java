package com.ravirane.smartguess.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by ravirane on 2/8/16.
 */
public class SGEditText extends EditText {

    public SGEditText(Context context) {
        super(context);
    }

    public SGEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SGEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}

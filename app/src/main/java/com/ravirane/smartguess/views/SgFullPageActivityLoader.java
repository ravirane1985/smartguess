package com.ravirane.smartguess.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.ravirane.smartguess.R;

/**
 * Created by ravirane on 2/16/16.
 */
public class SgFullPageActivityLoader extends RelativeLayout{

    public SgFullPageActivityLoader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        loadView();
    }

    public SgFullPageActivityLoader(Context context, AttributeSet attrs) {
        super(context, attrs);
        loadView();
    }

    public SgFullPageActivityLoader(Context context) {
        super(context);
        loadView();
    }

    private void loadView() {
        inflate(getContext(), R.layout.layout_loader, this);
    }
}

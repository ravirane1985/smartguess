package com.ravirane.smartguess.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ravirane.smartguess.R;

/**
 * Created by ravirane on 1/31/16.
 */
public class LogoStar extends LinearLayout {

    private Integer rating = 0;

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
        invalidate();
        requestLayout();
    }

    public LogoStar(Context context) {
        super(context);
        loadView(null);
    }


    public LogoStar(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.LogoStar,
                0, 0);


        loadView(a);
    }

    public LogoStar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.LogoStar,
                0, 0);
        loadView(a);
    }


    private void loadView(TypedArray a) {
        setWillNotDraw(false);
        inflate(getContext(), R.layout.view_rating_star, this);
        if(a != null) {
            rating = a.getInteger(R.styleable.LogoStar_rating, 1);
            setRatings(rating);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        setRatings(rating);
    }

    private void setRatings(int i) {
        switch (i) {
            case 1 : setRatingOne(); break;
            case 2 : setRatingTwo(); break;
            case 3 : setRatingThree(); break;
            case 4 : setRatingFour(); break;
            case 5 : setRatingFive(); break;
        }
    }

    private void setRatingOne() {
        ImageView star1 = (ImageView) findViewById(R.id.imgStar1);
        star1.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.star));
        ImageView star2 = (ImageView) findViewById(R.id.imgStar2);
        star2.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.emptystar));
        ImageView star3 = (ImageView) findViewById(R.id.imgStar3);
        star3.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.emptystar));
        ImageView star4 = (ImageView) findViewById(R.id.imgStar4);
        star4.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.emptystar));
        ImageView star5 = (ImageView) findViewById(R.id.imgStar5);
        star5.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.emptystar));
    }
    private void setRatingTwo() {
        ImageView star1 = (ImageView) findViewById(R.id.imgStar1);
        star1.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.star));
        ImageView star2 = (ImageView) findViewById(R.id.imgStar2);
        star2.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.star));
        ImageView star3 = (ImageView) findViewById(R.id.imgStar3);
        star3.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.emptystar));
        ImageView star4 = (ImageView) findViewById(R.id.imgStar4);
        star4.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.emptystar));
        ImageView star5 = (ImageView) findViewById(R.id.imgStar5);
        star5.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.emptystar));
    }
    private void setRatingThree() {
        ImageView star1 = (ImageView) findViewById(R.id.imgStar1);
        star1.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.star));
        ImageView star2 = (ImageView) findViewById(R.id.imgStar2);
        star2.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.star));
        ImageView star3 = (ImageView) findViewById(R.id.imgStar3);
        star3.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.star));
        ImageView star4 = (ImageView) findViewById(R.id.imgStar4);
        star4.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.emptystar));
        ImageView star5 = (ImageView) findViewById(R.id.imgStar5);
        star5.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.emptystar));
    }
    private void setRatingFour() {
        ImageView star1 = (ImageView) findViewById(R.id.imgStar1);
        star1.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.star));
        ImageView star2 = (ImageView) findViewById(R.id.imgStar2);
        star2.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.star));
        ImageView star3 = (ImageView) findViewById(R.id.imgStar3);
        star3.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.star));
        ImageView star4 = (ImageView) findViewById(R.id.imgStar4);
        star4.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.star));
        ImageView star5 = (ImageView) findViewById(R.id.imgStar5);
        star5.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.emptystar));
    }
    private void setRatingFive() {
        ImageView star1 = (ImageView) findViewById(R.id.imgStar1);
        star1.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.star));
        ImageView star2 = (ImageView) findViewById(R.id.imgStar2);
        star2.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.star));
        ImageView star3 = (ImageView) findViewById(R.id.imgStar3);
        star3.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.star));
        ImageView star4 = (ImageView) findViewById(R.id.imgStar4);
        star4.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.star));
        ImageView star5 = (ImageView) findViewById(R.id.imgStar5);
        star5.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.star));
    }

}

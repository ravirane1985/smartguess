package com.ravirane.smartguess.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ravirane.smartguess.R;
import com.ravirane.smartguess.model.GameQuestion;

import java.util.List;

/**
 * Created by ravirane on 12/21/15.
 */
public class GameGridSelectorAdapter extends RecyclerView.Adapter<GameGridSelectorAdapter.ViewHolder> {

    List<GameQuestion> gameQuestionList;

    public GameGridSelectorAdapter(List<GameQuestion> gameQuestionList) {
        this.gameQuestionList = gameQuestionList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView gameGridText;
        public ImageView gameGridPic;
        public ViewHolder(CardView v) {
            super(v);
            gameGridText = (TextView) v.findViewById(R.id.game_grid_text);
            gameGridPic = (ImageView) v.findViewById(R.id.game_grid_pic);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView cardView = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.card_game_grid_selector
                ,null);
        return new ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GameQuestion gameQuestion = gameQuestionList.get(position);
        holder.gameGridText.setText(Integer.toString(gameQuestion.questionComplexity));
        Glide.with(holder.gameGridPic.getContext()).load(gameQuestion.questionImg).into(holder.gameGridPic);
    }

    @Override
    public int getItemCount() {
        return gameQuestionList.size();
    }
}

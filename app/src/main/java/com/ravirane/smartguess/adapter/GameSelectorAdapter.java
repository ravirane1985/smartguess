package com.ravirane.smartguess.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ravirane.smartguess.GameGridActivity;
import com.ravirane.smartguess.R;
import com.ravirane.smartguess.model.Game;
import com.ravirane.smartguess.model.GameQuestion;
import com.ravirane.smartguess.parser.GameQuestionParser;
import com.ravirane.smartguess.util.HTTPConnect;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ravirane on 12/15/15.
 */
public class GameSelectorAdapter extends RecyclerView.Adapter<GameSelectorAdapter.ViewHolder>{



    private List<Game> games;
    private Context context;

    public GameSelectorAdapter() {
    }

    public GameSelectorAdapter(Context context, List<Game> games) {
        this.games = games;
        this.context = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;
        public ViewHolder(CardView v) {
            super(v);
            cardView = v;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView cv = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.card_game_selector,parent,false);
        return new ViewHolder(cv);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CardView cardView = holder.cardView;
        if(games.get(position).isEnable == 1) {
            cardView.setCardBackgroundColor(Color.GREEN);
        } else {
            cardView.setCardBackgroundColor(Color.GRAY);
        }
        ImageView imageView = (ImageView)cardView.findViewById(R.id.info_image);
        Glide.with(imageView.getContext()).load(games.get(position).gameLogo).into(imageView);
        imageView.setContentDescription(games.get(position).gameName);
        TextView textView = (TextView)cardView.findViewById(R.id.info_text);
        textView.setText(games.get(position).gameName);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new HttpAsyncTaskGameQuestionLoader(view.getContext()).execute("http://192.168.1.2:8000/game/gameq");
            }
        });
    }

    @Override
    public int getItemCount() {
        return games.size();
    }


    private class HttpAsyncTaskGameQuestionLoader extends AsyncTask<String, Void, String> {

       // Activity activiy;
        Context context;

        public HttpAsyncTaskGameQuestionLoader(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... urls) {
            HTTPConnect connect = new HTTPConnect();
            String jsonResp = connect.GET(urls[0]);
            return jsonResp;
        }

        @Override
        protected void onPostExecute(String jsonResp) {
            if(jsonResp != null) {
                ArrayList<GameQuestion> gameQuestions = GameQuestionParser.getGameQuestions(jsonResp);
                GameGridSelectorAdapter gameGridSelectorAdapter = new GameGridSelectorAdapter(gameQuestions);
                Intent intent = new Intent(context, GameGridActivity.class);
                intent.putExtra("gameQuestions", gameQuestions);
                context.startActivity(intent);
            } else {
                Toast toast = Toast.makeText(context, "Please check Internet Connection", Toast.LENGTH_LONG);
                toast.show();
            }
        }
    }

}

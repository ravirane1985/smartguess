package com.ravirane.smartguess.dao;

import android.content.ContentValues;
import android.database.Cursor;

import com.ravirane.smartguess.model.GameHint;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ravirane on 2/18/16.
 */
public class GameHintDao extends BaseDao{

    private String tableName = "GAMEHINT";
    private String columnString = " (gameId INTEGER,questionId INTEGER,hintId INTEGER,hint TEXT)";

    public GameHintDao() {
        super.columnString = columnString;
        super.tableName = tableName;
    }

    public List<GameHint> getGameHints(int gameId, int questionId) {
            init();
            List<GameHint> gameHints = new ArrayList<GameHint>();
            Cursor mCursor = db.query(tableName, null, "gameId = ? and questionId = ?", new String[]{Integer.toString(gameId), Integer.toString(questionId)}, null,
                    null, "hintId ASC", null);
            if(mCursor.moveToFirst()) {
                do {
                    GameHint gameHint = new GameHint();
                    gameHint.gameId = mCursor.getInt(mCursor.getColumnIndexOrThrow("gameId"));
                    gameHint.questionId = mCursor.getInt(mCursor.getColumnIndexOrThrow("questionId"));
                    gameHint.hintId = mCursor.getInt(mCursor.getColumnIndexOrThrow("hintId"));
                    gameHint.hint = mCursor.getString(mCursor.getColumnIndexOrThrow("hint"));
                    gameHints.add(gameHint);
                } while (mCursor.moveToNext());
            }
            return gameHints;
    }

    public void saveGameHints(List<GameHint> gameHints) {
        init();
        for (GameHint gameHint : gameHints){
            saveGameHint(gameHint);
        }
    }

    public void saveGameHint(GameHint gameHint) {
        ContentValues values = new ContentValues();
        values.put("gameId",gameHint.gameId);
        values.put("questionId",gameHint.questionId);
        values.put("hintId",gameHint.hintId);
        values.put("hint", gameHint.hint);
        db.insert(tableName, null, values);
    }

    public int getHintCount(int gameId, int questionId) {
        init();
        int hintCount = 0;
        Cursor mCount= db.rawQuery("select count(*) from " + tableName +
                " where gameId=" + gameId + " and questionId =" + questionId, null);
        mCount.moveToFirst();
        hintCount= mCount.getInt(0);
        mCount.close();
        return hintCount;
    }

}

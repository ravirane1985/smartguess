package com.ravirane.smartguess.dao;

import android.content.ContentValues;
import android.database.Cursor;

import com.ravirane.smartguess.model.GameQuestion;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ravirane on 2/18/16.
 */
public class GameQuestionDao extends BaseDao {

    private String tableName = "GAMEQUESTION";
    private String columnString = " ( gameId INTEGER,questionId INTEGER,questionText TEXT,questionAnswerText TEXT,questionImg TEXT,questionComplexity INTEGER)";

    public GameQuestionDao() {
        super.columnString = columnString;
        super.tableName = tableName;
    }


    public GameQuestion getGameQuestion(int gameId, int questionId) {
        init();
        Cursor mCursor = db.query(tableName, null, "gameId = ? and questionId = ?", new String[] {Integer.toString(gameId), Integer.toString(questionId)}, null,
                null, null, null);
        GameQuestion gameQuestion = null;
        if(mCursor.moveToFirst()) {
            gameQuestion = new GameQuestion();
            gameQuestion.gameId = mCursor.getInt(mCursor.getColumnIndexOrThrow("gameId"));
            gameQuestion.questionId = mCursor.getInt(mCursor.getColumnIndexOrThrow("questionId"));
            gameQuestion.questionText = mCursor.getString(mCursor.getColumnIndexOrThrow("questionText"));
            gameQuestion.questionAnswerText = mCursor.getString(mCursor.getColumnIndexOrThrow("questionAnswerText"));
            gameQuestion.questionComplexity = mCursor.getInt(mCursor.getColumnIndexOrThrow("questionComplexity"));
            gameQuestion.questionImg = mCursor.getString(mCursor.getColumnIndexOrThrow("questionImg"));
        }
        return gameQuestion;
    }

    public List<GameQuestion> getGameQuestions(int gameId) {
        init();
        List<GameQuestion> gameQuestions = new ArrayList<GameQuestion>();
        Cursor mCursor = db.query(tableName, null, "gameId = ?", new String[] {Integer.toString(gameId)}, null,
                null, "questionId ASC", null);
        if(mCursor.moveToFirst()) {
            do {
                GameQuestion gameQuestion = new GameQuestion();
                gameQuestion.gameId = mCursor.getInt(mCursor.getColumnIndexOrThrow("gameId"));
                gameQuestion.questionId = mCursor.getInt(mCursor.getColumnIndexOrThrow("questionId"));
                gameQuestion.questionText = mCursor.getString(mCursor.getColumnIndexOrThrow("questionText"));
                gameQuestion.questionAnswerText = mCursor.getString(mCursor.getColumnIndexOrThrow("questionAnswerText"));
                gameQuestion.questionComplexity = mCursor.getInt(mCursor.getColumnIndexOrThrow("questionComplexity"));
                gameQuestion.questionImg = mCursor.getString(mCursor.getColumnIndexOrThrow("questionImg"));
                gameQuestions.add(gameQuestion);
            } while (mCursor.moveToNext());
        }
        return gameQuestions;
    }

    public void saveGameQuestions(List<GameQuestion> gameQuestions) {
        init();
        for (GameQuestion gameQuestion : gameQuestions){
            saveGameQuestion(gameQuestion);
        }
    }

    public void saveGameQuestion(GameQuestion gameQuestion) {
        ContentValues values = new ContentValues();
        values.put("gameId",gameQuestion.gameId);
        values.put("questionId",gameQuestion.questionId);
        values.put("questionAnswerText",gameQuestion.questionAnswerText);
        values.put("questionText",gameQuestion.questionText );
        values.put("questionImg",gameQuestion.questionImg);
        values.put("questionComplexity",gameQuestion.questionComplexity);
        db.insert(tableName, null, values);
    }

}

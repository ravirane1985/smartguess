package com.ravirane.smartguess.dao;

import android.content.ContentValues;
import android.database.Cursor;

import com.ravirane.smartguess.model.UserGame;

/**
 * Created by ravirane on 2/18/16.
 */
public class UserGameDao extends BaseDao{

    public UserGameDao() {
        tableName = "USERGAME";
        columnString = " ( gameId INTEGER,isEnabled INTEGER )";
    }

    public UserGame getUserGame(int gameId) {
        init();
        UserGame userGame = null;
        Cursor mCursor = db.query(tableName, null, "gameId = ?", new String[] {Integer.toString(gameId)}, null,
                null, null, null);
        if(mCursor.moveToFirst()) {
            userGame = new UserGame();
            userGame.isEnabled = mCursor.getInt(mCursor.getColumnIndexOrThrow("isEnabled"));
            userGame.gameId = mCursor.getInt(mCursor.getColumnIndexOrThrow("gameId"));

        }
        return userGame;
    }

    public void upsertUserGame(UserGame userGame) {
        init();
        UserGame uGame = getUserGame(userGame.gameId);
        if(uGame != null) {
            ContentValues values = new ContentValues();
            values.put("isEnabled", userGame.isEnabled);
            db.update(tableName,values,"gameId = ?", new String[] {Integer.toString(userGame.gameId)});
        } else {
            saveUserGame(userGame);
        }
    }

    public void saveUserGame(UserGame userGame) {
        init();
        ContentValues values = new ContentValues();
        values.put("gameId",userGame.gameId);
        values.put("isEnabled",userGame.isEnabled);
        db.insert(tableName, null, values);
    }
}

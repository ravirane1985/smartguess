package com.ravirane.smartguess.dao;

import android.database.sqlite.SQLiteDatabase;

import com.ravirane.smartguess.util.DatabaseHelper;

/**
 * Created by ravirane on 8/16/15.
 */
public class BaseDao {


    public static SQLiteDatabase db;
    public String tableName;
    public String columnString;

    public void dropTable() {
        init();
        String gameTbl = "DROP TABLE " + tableName;
        db.execSQL(gameTbl);
    }

    public void createTable() {
        init();
        String gameTbl = "CREATE TABLE IF NOT EXISTS " + tableName + columnString;
        db.execSQL(gameTbl);
    }

    public static void init() {
        db = DatabaseHelper.db;
    }

}

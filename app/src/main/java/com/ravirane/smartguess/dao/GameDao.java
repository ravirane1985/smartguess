package com.ravirane.smartguess.dao;

import android.content.ContentValues;
import android.database.Cursor;

import com.ravirane.smartguess.model.Game;
import com.ravirane.smartguess.model.UserGame;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ravirane on 12/18/15.
 */
public class GameDao extends BaseDao {

    public GameDao() {
        tableName = "GAME";
        columnString  = " ( gameId INTEGER,gameName TEXT,gameLogo TEXT,isEnable TEXT,logoCount INTEGER,gameRating INTEGER)";
    }

    public void saveGames(List<Game> gameList) {
        init();
        for(Game game : gameList) {
            saveGame(game);
        }
    }

    private void saveGame(Game game) {
        ContentValues values = new ContentValues();
        values.put("gameId",game.gameId);
        values.put("gameRating",game.gameRating);
        values.put("logoCount",game.logoCount);
        values.put("gameName",game.gameName );
        values.put("gameLogo",game.gameLogo);
        db.insert(tableName, null, values);

        UserGameDao userGameDao = new UserGameDao();
        UserGame userGame = userGameDao.getUserGame(game.gameId);
        if(userGame != null) {
            game.isEnable = 1;
        }
    }

    public void clearGames() {
        init();
        String deletQuery = "DELETE FROM GAME";
        db.execSQL(deletQuery);
    }

    public Game getGame(int gameId) {
        init();
        Game game = null;
        Cursor mCursor = db.query(tableName,null , "gameId = ?", new String[] {Integer.toString(gameId)},
                null, null, null, null);
        if(mCursor.moveToFirst()) {
            game = new Game();
            game.gameId = mCursor.getInt(mCursor.getColumnIndexOrThrow("gameId"));
            game.gameName = mCursor.getString(mCursor.getColumnIndexOrThrow("gameName"));
            game.gameLogo = mCursor.getString(mCursor.getColumnIndexOrThrow("gameLogo"));
            game.gameRating = mCursor.getInt(mCursor.getColumnIndexOrThrow("gameRating"));
            game.logoCount = mCursor.getInt(mCursor.getColumnIndexOrThrow("logoCount"));
        }
        return game;
    }

    public List<Game> getGames() {
        init();
        UserGameDao userGameDao = new UserGameDao();
        List<Game> games = new ArrayList<>();
        Game game = null;
        Cursor mCursor = db.query(tableName,null , null,null,
                null, null, "gameId ASC", null);
        if (mCursor.moveToFirst()) {
            do {
                game = new Game();
                game.gameId = mCursor.getInt(mCursor.getColumnIndexOrThrow("gameId"));
                game.gameName = mCursor.getString(mCursor.getColumnIndexOrThrow("gameName"));
                game.gameLogo = mCursor.getString(mCursor.getColumnIndexOrThrow("gameLogo"));
                game.gameRating = mCursor.getInt(mCursor.getColumnIndexOrThrow("gameRating"));
                game.logoCount = mCursor.getInt(mCursor.getColumnIndexOrThrow("logoCount"));
                UserGame userGame = userGameDao.getUserGame(game.gameId);
                if(userGame == null) {
                    game.isEnable = 0;
                } else {
                    game.isEnable = userGame.isEnabled;
                }
                games.add(game);
            }
            while(mCursor.moveToNext());
        }
        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.close();
        }
        return games;
    }

}

package com.ravirane.smartguess.dao;

import android.content.ContentValues;
import android.database.Cursor;

import com.ravirane.smartguess.model.UserQuestion;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ravirane on 2/18/16.
 */
public class UserQuestionDao extends BaseDao{

    private String tableName = "USERQUESTION";
    private String columnString = " ( gameId INTEGER, questionId INTEGER,isAnsweredCorrectly INTEGER,currentHintId INTEGER)";

    public UserQuestionDao() {
        super.tableName = tableName;
        super.columnString = columnString;
    }

    public int getCorrectQuestionsCount(int gameId) {
        init();
        int questionsAnsweredCorrectly = 0;
        Cursor mCount= db.rawQuery("select count(*) from " + tableName +
                " where gameId='" + gameId + "' and isAnsweredCorrectly = 1", null);
        mCount.moveToFirst();
        questionsAnsweredCorrectly= mCount.getInt(0);
        mCount.close();
        return questionsAnsweredCorrectly;
    }

    public UserQuestion getUserQuestion(int gameId, int questionId) {
        init();
        Cursor mCursor = db.query(tableName, null, "gameId = ? and questionId = ?", new String[]{Integer.toString(gameId), Integer.toString(questionId)}, null,
                null, null, null);
        UserQuestion userQuestion = null;
        if(mCursor.moveToFirst()) {
            userQuestion = new UserQuestion();
            userQuestion.gameId = mCursor.getInt(mCursor.getColumnIndexOrThrow("gameId"));
            userQuestion.questionId = mCursor.getInt(mCursor.getColumnIndexOrThrow("questionId"));
            userQuestion.isAnsweredCorrectly = mCursor.getInt(mCursor.getColumnIndexOrThrow("isAnsweredCorrectly"));
            userQuestion.currentHintId = mCursor.getInt(mCursor.getColumnIndexOrThrow("currentHintId"));
        }
        return userQuestion;
    }

    public List<UserQuestion> getUserQuestions(int gameId) {
        init();
        List<UserQuestion> userQuestions = new ArrayList<>();
        Cursor mCursor = db.query(tableName, null, "gameId = ?", new String[] {Integer.toString(gameId)}, null,
                null, null, null);
        if(mCursor.moveToFirst()) {
            do {
                UserQuestion userQuestion= new UserQuestion();
                userQuestion.gameId = mCursor.getInt(mCursor.getColumnIndexOrThrow("gameId"));
                userQuestion.questionId = mCursor.getInt(mCursor.getColumnIndexOrThrow("questionId"));
                userQuestion.isAnsweredCorrectly = mCursor.getInt(mCursor.getColumnIndexOrThrow("isAnsweredCorrectly"));
                userQuestion.currentHintId = mCursor.getInt(mCursor.getColumnIndexOrThrow("currentHintId"));
                userQuestions.add(userQuestion);
            } while (mCursor.moveToNext());
        }
        return userQuestions;
    }

    public void saveUserQuestions(List<UserQuestion> userQuestions) {
        init();
        for (UserQuestion userQuestion : userQuestions){
            saveUserQuestion(userQuestion);
        }
    }

    public void saveUserQuestion(UserQuestion userQuestion) {
        ContentValues values = new ContentValues();
        values.put("gameId",userQuestion.gameId);
        values.put("questionId",userQuestion.questionId);
        values.put("currentHintId",userQuestion.currentHintId);
        values.put("isAnsweredCorrectly", userQuestion.isAnsweredCorrectly);
        db.insert(tableName, null, values);
    }

    public void updateUserQuestion(UserQuestion userQuestion) {
        init();
        ContentValues values = new ContentValues();
        values.put("currentHintId",userQuestion.currentHintId);
        values.put("isAnsweredCorrectly", userQuestion.isAnsweredCorrectly);
        db.update(tableName, values, "gameId = ? and questionId = ?", new String[]{Integer.toString(userQuestion.gameId), Integer.toString(userQuestion.questionId)});
    }



}

package com.ravirane.smartguess.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String dbName = "games.db";
    public static SQLiteDatabase db;

    private static DatabaseHelper databaseHelper;

    private DatabaseHelper(Context context) {
        super(context, dbName, null, 1);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS PROFILE");
        onCreate(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

      //  db.execSQL(getProfileDDL());
    }

    public static DatabaseHelper getInstance(Context context) {
        if(databaseHelper == null ) {
            databaseHelper = new DatabaseHelper(context);
        }
        return databaseHelper;
    }



    public static SQLiteDatabase openDb() {
      db = getInstance(null).getWritableDatabase();
        return db;
    }

    public static void closeDb() {
         getInstance(null).close();
    }


}

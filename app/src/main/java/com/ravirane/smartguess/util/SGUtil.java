package com.ravirane.smartguess.util;

import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by ravirane on 2/8/16.
 */
public class SGUtil {

    public void setCutomFont(TextView tv, AttributeSet attrs) {

    }

    public static int getCredits(int complexity) {
        return complexity * 10;
    }

    public static int getScore(int complexity, int hintsUsed) {
         int credits = getCredits(complexity);
         return credits - ((credits * hintsUsed * 10)/100);
    }

}

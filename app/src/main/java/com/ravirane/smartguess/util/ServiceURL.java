package com.ravirane.smartguess.util;

/**
 * Created by ravirane on 1/23/16.
 */
public class ServiceURL {

    public static String getBaseURL() {
        return "https://api.backendless.com/v1/data/";
    }

    public static String getFinalURL(String urlContext) {
        StringBuilder url = new StringBuilder();
        url.append("https://api.backendless.com/v1/data/");
        url.append(urlContext);
        return url.toString();
    }

}

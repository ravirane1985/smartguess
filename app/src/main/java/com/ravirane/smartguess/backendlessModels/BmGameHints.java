package com.ravirane.smartguess.backendlessModels;

import com.ravirane.smartguess.model.GameHint;

import java.util.List;

/**
 * Created by ravirane on 3/7/16.
 */
public class BmGameHints {
    public Integer offset;
    public List<GameHint> data;
    public Integer nextPage;
    public Integer totalObjects;
}

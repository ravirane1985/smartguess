package com.ravirane.smartguess.backendlessModels;

import com.ravirane.smartguess.model.GameQuestion;

import java.util.List;

public class BmGameQuestion {

    public Integer offset;
    public List<GameQuestion> data;
    public Integer nextPage;
    public Integer totalObjects;

}

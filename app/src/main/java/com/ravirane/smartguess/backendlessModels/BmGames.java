package com.ravirane.smartguess.backendlessModels;

import com.ravirane.smartguess.model.Game;

import java.util.List;

/**
 * Created by ravirane on 2/13/16.
 */
public class BmGames {

    public Integer offset;
    public List<Game> data;
    public Integer nextPage;
    public Integer totalObjects;

}

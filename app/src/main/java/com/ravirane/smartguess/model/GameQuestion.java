package com.ravirane.smartguess.model;

/**
 * Created by ravirane on 12/22/15.
 */
public class GameQuestion {

    public int gameId;
    public int questionId;
    public String questionText;
    public String questionAnswerText;
    public int questionComplexity;
    public String questionImg;

}

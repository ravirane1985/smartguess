package com.ravirane.smartguess.model;

/**
 * Created by ravirane on 2/18/16.
 */
public class GameHint {

    public int gameId;
    public int questionId;
    public int hintId;
    public String hint;

}

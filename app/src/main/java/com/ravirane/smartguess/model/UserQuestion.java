package com.ravirane.smartguess.model;

/**
 * Created by ravirane on 2/18/16.
 */
public class UserQuestion {

    public int gameId;
    public int questionId;
    public int isAnsweredCorrectly;
    public int currentHintId;

}

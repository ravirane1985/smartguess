package com.ravirane.smartguess.model;

/**
 * Created by ravirane on 12/15/15.
 */
public class Game {

    public int gameId;
    public String gameName;
    public String gameLogo;
    public int isEnable;
    public int logoCount;
    public int gameRating;

}

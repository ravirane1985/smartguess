package com.ravirane.smartguess.parser;

import com.ravirane.smartguess.model.Game;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ravirane on 12/20/15.
 */
public class GameParser {

    public static List<Game> getGames(String games) {
        List<Game> gameList = new ArrayList<>();
        try {
            JSONObject jsonRootObject = new JSONObject(games);
            JSONArray jsonArray = jsonRootObject.optJSONArray("games");
            for(int i = 0; i < jsonArray.length(); i++ ) {
                Game game = getGame(jsonArray.getJSONObject(i));
                gameList.add(game);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return gameList;
    }

    public static Game getGame(JSONObject job) {
        Game game = new Game();
        if(job.optString("gameId") != null) {
            game.gameId = job.optInt("gameId");
        }
        if(job.optString("gameLogo") != null) {
            game.gameLogo = job.optString("gameLogo");
        }
        if(job.optString("gameName") != null) {
            game.gameName = job.optString("gameName");
        }

        return game;
    }

}

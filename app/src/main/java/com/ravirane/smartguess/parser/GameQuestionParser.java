package com.ravirane.smartguess.parser;

import com.ravirane.smartguess.model.GameQuestion;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ravirane on 12/22/15.
 */
public class GameQuestionParser {

    public static ArrayList<GameQuestion> getGameQuestions(String gameQuestions) {
        ArrayList<GameQuestion> gameQuestionList = new ArrayList<>();
        try {
            JSONObject jsonRootObject = new JSONObject(gameQuestions);
            JSONArray jsonArray = jsonRootObject.optJSONArray("gamequestion");
            for(int i = 0; i < jsonArray.length(); i++ ) {
                GameQuestion gameQuestion = getGameQuestion(jsonArray.getJSONObject(i));
                gameQuestionList.add(gameQuestion);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gameQuestionList;
    }

    public static GameQuestion getGameQuestion(JSONObject job) {
        GameQuestion gameQuestion = new GameQuestion();

        return gameQuestion;
    }

}

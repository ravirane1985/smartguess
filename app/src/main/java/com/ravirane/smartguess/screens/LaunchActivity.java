package com.ravirane.smartguess.screens;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;

import com.ravirane.smartguess.R;
import com.ravirane.smartguess.common.contract.BaseActivity;
import com.ravirane.smartguess.common.contract.IResponseAction;
import com.ravirane.smartguess.common.contract.IServiceResponseListener;
import com.ravirane.smartguess.common.impl.ResponseActionImpl;
import com.ravirane.smartguess.common.impl.ServiceHandler;
import com.ravirane.smartguess.common.requests.GameGuessRequest;
import com.ravirane.smartguess.dao.GameDao;
import com.ravirane.smartguess.dao.GameHintDao;
import com.ravirane.smartguess.dao.GameQuestionDao;
import com.ravirane.smartguess.dao.UserGameDao;
import com.ravirane.smartguess.dao.UserQuestionDao;
import com.ravirane.smartguess.model.Game;
import com.ravirane.smartguess.util.DatabaseHelper;

import java.util.ArrayList;

public class LaunchActivity extends BaseActivity implements IServiceResponseListener{

    GameDao gameDao = null;
    UserGameDao userGameDao = null;
    GameQuestionDao gameQuestionDao = null;
    UserQuestionDao userQuestionDao = null;
    GameHintDao gameHintDao = null;
    private ProgressBar mProgress;
    private int mProgressStatus = 0;
    LaunchActivity launchActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        launchActivity = this;
        DatabaseHelper.getInstance(getApplicationContext());
        DatabaseHelper.openDb();
        createTables();
        if(gameDao.getGames().size() > 0) {
            DatabaseHelper.closeDb();
            Intent intent = new Intent(LaunchActivity.this, GameSelectorActivity.class);
            LaunchActivity.this.startActivity(intent);
        } else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    GameGuessRequest request = new GameGuessRequest(GameGuessRequest.method.GET,
                            GameGuessRequest.action.GET_GAMES,null);
                    ServiceHandler.getInstance().processRequest(request, launchActivity);
                }
            }).start();
        }
    }

    public void createTables() {
        gameDao = new GameDao();
        gameDao.dropTable();
        gameDao.createTable();
        userGameDao = new UserGameDao();
        userGameDao.dropTable();
        userGameDao.createTable();
        gameQuestionDao = new GameQuestionDao();
        gameQuestionDao.dropTable();
        gameQuestionDao.createTable();
        userQuestionDao = new UserQuestionDao();
        userQuestionDao.dropTable();
        userQuestionDao.createTable();
        gameHintDao = new GameHintDao();
        gameHintDao.dropTable();
        gameHintDao.createTable();
    }

    private void nextScreen() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_launch, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onSuccess(IResponseAction responseAction) {
        Object obj = ((ResponseActionImpl) responseAction).responseActionMap.get("games");
        ArrayList<Game> games = (ArrayList<Game>) obj;
        DatabaseHelper.openDb();
        gameDao.saveGames(games);
        DatabaseHelper.closeDb();
        Intent intent = new Intent(LaunchActivity.this, GameSelectorActivity.class);
        LaunchActivity.this.startActivity(intent);
    }

    @Override
    public void onFailure(IResponseAction responseAction) {

    }
}

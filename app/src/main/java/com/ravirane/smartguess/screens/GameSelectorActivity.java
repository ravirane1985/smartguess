package com.ravirane.smartguess.screens;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ravirane.smartguess.R;
import com.ravirane.smartguess.common.contract.IResponseAction;
import com.ravirane.smartguess.common.contract.IServiceResponseListener;
import com.ravirane.smartguess.common.impl.ResponseActionImpl;
import com.ravirane.smartguess.common.impl.ServiceHandler;
import com.ravirane.smartguess.common.requests.GameGuessRequest;
import com.ravirane.smartguess.dao.GameDao;
import com.ravirane.smartguess.dao.UserQuestionDao;
import com.ravirane.smartguess.model.Game;
import com.ravirane.smartguess.service.GameQuestionLoader;
import com.ravirane.smartguess.util.DatabaseHelper;
import com.ravirane.smartguess.views.LogoStar;

import java.util.ArrayList;
import java.util.List;


public class GameSelectorActivity extends AppCompatActivity implements IServiceResponseListener {

    GameDao gameDao = null;
    List<Game> gameList = new ArrayList<>();
    GameSelectorActivity gameSelectorActivity;
    private ListAdapter adapter;
    ListView lv = null;
    Menu menu;

    private class ListAdapter extends BaseAdapter {

        private Activity activity;

        public ListAdapter(Activity activity) {
            this.activity = activity;
        }

        @Override
        public int getCount() {
            return gameList.size();
        }

        @Override
        public Object getItem(int position) {
            return gameList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final Game game = gameList.get(position);
            if (convertView == null) {
                int layout_id = R.layout.row_game_new;
                LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(layout_id, null);
            }
            ImageView imgGameLogo = (ImageView) convertView.findViewById(R.id.imgGameLogo);
            TextView txtGameTitle = (TextView) convertView.findViewById(R.id.txtGameTitle);
            LogoStar logoStar = (LogoStar) convertView.findViewById(R.id.ratingBar);
            final ProgressBar pb = (ProgressBar) convertView.findViewById(R.id.downloadProgress);
            final ImageView imgGameStatus = (ImageView) convertView.findViewById(R.id.imgGameStatus);

            ProgressBar gameProgress = (ProgressBar) convertView.findViewById(R.id.gameProgressBar);
            TextView txtPrcntCount = (TextView) convertView.findViewById(R.id.txtPrcntCount);
            TextView txtGameScore = (TextView) convertView.findViewById(R.id.txtGameScore);


            imgGameLogo.setImageResource(R.drawable.gamecar);
            txtGameTitle.setText(game.gameName);
            logoStar.setRating(game.gameRating);

            DatabaseHelper.openDb();
            UserQuestionDao userQuestionDao = new UserQuestionDao();
            int successCount = userQuestionDao.getCorrectQuestionsCount(game.gameId);
            DatabaseHelper.closeDb();
            gameProgress.setMax(game.logoCount);
            gameProgress.setProgress(successCount);
            int percntCompletion = ( successCount * 100 )/game.logoCount;
            txtPrcntCount.setText(percntCompletion + "%");

            if(game.isEnable == 0) {
                txtGameScore.setText("Download Game and start playing");
            } else {
                txtGameScore.setText("0 Credits");
            }

            if (game.isEnable == 1) {
                imgGameStatus.setImageResource(R.drawable.greencheck);
            } else {
                imgGameStatus.setImageResource(R.drawable.downloadicon);
                imgGameStatus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                            imgGameStatus.setOnClickListener(null);
                            imgGameStatus.setVisibility(View.INVISIBLE);
                            pb.setVisibility(View.VISIBLE);
                            GameQuestionLoader gameQuestionLoader = new GameQuestionLoader(game.gameId,
                                    imgGameStatus, pb, gameSelectorActivity);
                            gameQuestionLoader.loadGame();
                       }
                });
            }
            return convertView;
        }

        public  synchronized void refreshAdapter(List<Game> games) {
            gameList.clear();
            gameList.addAll(games);
            notifyDataSetChanged();
        }

        @Override
        public boolean isEnabled(int position) {
            Game game = gameList.get(position);
            if(game.isEnable == 1) {
                return true;
            } else {
                return false;
            }

        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_selector);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        gameSelectorActivity = this;
        lv = (ListView) findViewById(R.id.listGames);
        DatabaseHelper.openDb();
        gameDao = new GameDao();
        gameList = gameDao.getGames();
        DatabaseHelper.closeDb();
        adapter = new ListAdapter(GameSelectorActivity.this);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Game game = (Game) parent.getItemAtPosition(position);
                    Intent intent = new Intent(GameSelectorActivity.this,LogoSelectorActivity.class);
                    intent.putExtra("gameId",game.gameId);
                    startActivity(intent);

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game_selector, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
        //    showSpinner(true, true);
            setRefreshActionButtonState(true);
            refreshGameList();

        }
        return super.onOptionsItemSelected(item);
    }

    private void refreshGameList() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                GameGuessRequest request = new GameGuessRequest(GameGuessRequest.method.GET,
                        GameGuessRequest.action.GET_GAMES,null);
                ServiceHandler.getInstance().processRequest(request, gameSelectorActivity);
            }
        }).start();
    }

    public void showSpinner(final boolean show, boolean fromThread) {
      if(fromThread) {
          runOnUiThread(new Runnable() {
              @Override
              public void run() {
                  findViewById(R.id.activityIndicator).setVisibility(show ? View.VISIBLE : View.GONE);
              }
          });
      } else {
          findViewById(R.id.activityIndicator).setVisibility(show ? View.VISIBLE : View.GONE);
      }
    }

    @Override
    public void onSuccess(IResponseAction responseAction) {
        Object obj = ((ResponseActionImpl) responseAction).responseActionMap.get("games");
        final ArrayList<Game> games = (ArrayList<Game>) obj;
        DatabaseHelper.openDb();
        gameDao = new GameDao();
        gameDao.clearGames();
        gameDao.saveGames(games);
        DatabaseHelper.closeDb();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.refreshAdapter(games);
                setRefreshActionButtonState(false);
            }
        });
    }

    @Override
    public void onFailure(IResponseAction responseAction) {

    }

    public void setRefreshActionButtonState(final boolean refreshing) {
        if (menu != null) {
            final MenuItem refreshItem = menu
                    .findItem(R.id.action_refresh);
            if (refreshItem != null) {
                if (refreshing) {
                    refreshItem.setActionView(R.layout.actionbar_indeterminate_progress);
                } else {
                    refreshItem.setActionView(null);
                }
            }
        }
    }

    public synchronized void updateGameQuestionLoadSuccess(final ImageView imgGameStatus, final ProgressBar pb) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imgGameStatus.setImageResource(R.drawable.greencheck);
                imgGameStatus.setVisibility(View.VISIBLE);
                pb.setVisibility(View.INVISIBLE);
                DatabaseHelper.openDb();
                gameDao = new GameDao();
                gameList = gameDao.getGames();
                DatabaseHelper.closeDb();
                lv.invalidateViews();
            }
        });
    }

}

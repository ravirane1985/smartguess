package com.ravirane.smartguess.screens;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.ravirane.smartguess.R;
import com.ravirane.smartguess.dao.GameDao;
import com.ravirane.smartguess.dao.GameHintDao;
import com.ravirane.smartguess.dao.GameQuestionDao;
import com.ravirane.smartguess.dao.UserQuestionDao;
import com.ravirane.smartguess.model.Game;
import com.ravirane.smartguess.model.GameHint;
import com.ravirane.smartguess.model.GameQuestion;
import com.ravirane.smartguess.model.UserQuestion;
import com.ravirane.smartguess.util.DatabaseHelper;
import com.ravirane.smartguess.util.SGUtil;
import com.ravirane.smartguess.views.LogoStar;

import java.util.List;

public class QuizSolutionActivity extends AppCompatActivity {

    List<GameHint> gameHints = null;
    int currentHint = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_solution);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.gameQuiztoolbar);
        setSupportActionBar(myToolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        Intent intent = getIntent();
        int gameId = intent.getIntExtra("gameId", 0);
        int qId = intent.getIntExtra("questionId",0);
        if(gameId != 0 && qId != 0) {
            DatabaseHelper.openDb();
            GameDao gameDao = new GameDao();
            Game game = gameDao.getGame(gameId);
            GameQuestionDao gameQuestionDao = new GameQuestionDao();
            GameQuestion gameQuestion = gameQuestionDao.getGameQuestion(gameId,qId);
            GameHintDao gameHintDao = new GameHintDao();
            gameHints = gameHintDao.getGameHints(gameId,qId);
            UserQuestionDao userQuestionDao = new UserQuestionDao();
            UserQuestion userQuestion = userQuestionDao.getUserQuestion(gameId, qId);
            DatabaseHelper.closeDb();
            TextView lblLogoQuestion = (TextView) findViewById(R.id.lblLogoQuestion);
            lblLogoQuestion.setText(gameQuestion.questionText);
            TextView txtLogoVal = (TextView) findViewById(R.id.txtLogoVal);
            txtLogoVal.setText(gameQuestion.questionAnswerText);
            LogoStar logoStar = (LogoStar) findViewById(R.id.customLogoStar);
            logoStar.setRating(gameQuestion.questionComplexity);
            TextView txtHint = (TextView) findViewById(R.id.txtHint);
            txtHint.setText(gameHints.get(currentHint).hint);
            TextView txtScore = (TextView) findViewById(R.id.txtScore);
            txtScore.setText("Earned Credits : " + Integer.toString(SGUtil.getScore(gameQuestion.questionComplexity, userQuestion.currentHintId)));

        } else {
            Log.e("Incorrect game","Incorrect game");
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quiz_solution, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void nextHint(View v) {
        currentHint++;
        if(currentHint < gameHints.size()) {
            TextView txtHint = (TextView) findViewById(R.id.txtHint);
            txtHint.setText(gameHints.get(currentHint).hint);
        } else {
            currentHint--;
        }
    }

    public void previousHint(View v) {
        currentHint--;
        if(currentHint >= 0) {
            TextView txtHint = (TextView) findViewById(R.id.txtHint);
            txtHint.setText(gameHints.get(currentHint).hint);
        } else {
            currentHint++;
        }
    }
}

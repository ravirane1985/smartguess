package com.ravirane.smartguess.screens;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ravirane.smartguess.R;
import com.ravirane.smartguess.dao.GameHintDao;
import com.ravirane.smartguess.dao.GameQuestionDao;
import com.ravirane.smartguess.dao.UserQuestionDao;
import com.ravirane.smartguess.model.GameHint;
import com.ravirane.smartguess.model.GameQuestion;
import com.ravirane.smartguess.model.UserQuestion;
import com.ravirane.smartguess.util.DatabaseHelper;
import com.ravirane.smartguess.util.Preferences;
import com.ravirane.smartguess.views.LogoStar;

import java.util.List;

public class QuizActivity extends AppCompatActivity {

    GameQuestion gameQuestion;
    UserQuestion userQuestion;
    List<GameHint> gameHints;
    int currentHint = 0;
    int gameId;
    int questionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_quiz_new);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.gameQuiztoolbar);
        setSupportActionBar(myToolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        gameId = getIntent().getIntExtra("gameId",0);
        questionId = getIntent().getIntExtra("questionId",0);

        if(gameId != 0 && questionId != 0) {
            Preferences.writeInteger(getApplicationContext(), "gameId", gameId);
            DatabaseHelper.openDb();
            GameQuestionDao gameQuestionDao = new GameQuestionDao();
            gameQuestion = gameQuestionDao.getGameQuestion(gameId, questionId);
            UserQuestionDao userQuestionDao = new UserQuestionDao();
            userQuestion = userQuestionDao.getUserQuestion(gameId, questionId);
            GameHintDao gameHintDao = new GameHintDao();
            gameHints = gameHintDao.getGameHints(gameId, questionId);
            DatabaseHelper.closeDb();
            TextView lblLogoQuestion = (TextView) findViewById(R.id.lblLogoQuestion);
            lblLogoQuestion.setText(gameQuestion.questionId + ". " + gameQuestion.questionText);
            LogoStar logoStar = (LogoStar) findViewById(R.id.customLogoStar);
            logoStar.setRating(gameQuestion.questionComplexity);
            EditText txtLogoVal = (EditText) findViewById(R.id.txtLogoVal);
            if(userQuestion.currentHintId != 999) {
                currentHint = userQuestion.currentHintId;
                TextView txtHint = (TextView) findViewById(R.id.txtHint);
                txtHint.setText(gameHints.get(currentHint).hint);
            }
        }





    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quiz, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void nextHint(View v) {
        currentHint++;
        if(currentHint < gameHints.size()) {
            TextView txtHint = (TextView) findViewById(R.id.txtHint);
            txtHint.setText(gameHints.get(currentHint).hint);
            saveUserQuestionForHint();
        } else {
            currentHint--;
        }
    }

    public void previousHint(View v) {
        currentHint--;
        if(currentHint >= 0) {
            TextView txtHint = (TextView) findViewById(R.id.txtHint);
            txtHint.setText(gameHints.get(currentHint).hint);
        } else {
            currentHint++;
        }
    }


    public void tryAnswer(View v) {
        Log.e("","Try Ansr");
        EditText txtLogoVal = (EditText) findViewById(R.id.txtLogoVal);
        String logoAnswr = txtLogoVal.getText().toString().trim();
        if(logoAnswr.length() == 0) {

        } else {
            if(logoAnswr.equalsIgnoreCase(gameQuestion.questionAnswerText)) {
               // AlertDialog.Builder builder = new AlertDialog.Builder(QuizActivity.this);
               // builder.setMessage("Some Message").setTitle("Some Title");
               // AlertDialog dialog = builder.create();
                final Dialog successDialog = new Dialog(QuizActivity.this);
                successDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                successDialog.setContentView(R.layout.success_dialog);
                successDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                successDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        Log.e("cancelled", "cancelled");
                        Intent intent = new Intent(QuizActivity.this, QuizSolutionActivity.class);
                        intent.putExtra("gameId", gameId);
                        intent.putExtra("questionId", questionId);
                        startActivity(intent);
                    }
                });
                ImageView dialogImg = (ImageView) successDialog.findViewById(R.id.successresult);
                dialogImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        successDialog.dismiss();
                        Log.e("cancelled", "cancelled");
                        Intent intent = new Intent(QuizActivity.this, QuizSolutionActivity.class);
                        intent.putExtra("gameId", gameId);
                        intent.putExtra("questionId", questionId);
                        startActivity(intent);
                    }
                });
                successDialog.show();
            } else {
                final Dialog successDialog = new Dialog(QuizActivity.this);
                successDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                successDialog.setContentView(R.layout.success_dialog);
                successDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                ImageView dialogImg = (ImageView) successDialog.findViewById(R.id.successresult);
                dialogImg.setImageResource(R.drawable.thumbsdown);
                dialogImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        successDialog.dismiss();
                        Log.e("close me","close me");
                    }
                });
                successDialog.show();
            }
        }
    }

    public void viewHints(View v) {
        saveUserQuestionForHint();
        TextView txtHint = (TextView) findViewById(R.id.txtHint);
        txtHint.setText(gameHints.get(currentHint).hint);
    }

    public void saveUserQuestionForHint() {
        userQuestion.currentHintId = currentHint;
        DatabaseHelper.openDb();
        UserQuestionDao userQuestionDao = new UserQuestionDao();
        userQuestionDao.updateUserQuestion(userQuestion);
        DatabaseHelper.closeDb();
    }

}

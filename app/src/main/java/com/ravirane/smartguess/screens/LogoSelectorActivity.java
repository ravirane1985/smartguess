package com.ravirane.smartguess.screens;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.ravirane.smartguess.R;
import com.ravirane.smartguess.dao.GameQuestionDao;
import com.ravirane.smartguess.dao.UserQuestionDao;
import com.ravirane.smartguess.model.GameQuestion;
import com.ravirane.smartguess.model.UserQuestion;
import com.ravirane.smartguess.util.DatabaseHelper;
import com.ravirane.smartguess.util.Preferences;
import com.ravirane.smartguess.util.SGUtil;
import com.ravirane.smartguess.views.LogoStar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ravirane on 1/31/16.
 */
public class LogoSelectorActivity extends AppCompatActivity {

    List<GameQuestion> logoList = new ArrayList<>();
    Map<Integer,UserQuestion> userQuestionMap;


    private class ListAdapter extends BaseAdapter {

        private Activity activity;

        public ListAdapter(Activity activity) {
            this.activity = activity;
        }

        @Override
        public int getCount() {
            return logoList.size();
        }

        @Override
        public Object getItem(int position) {
            return logoList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            GameQuestion gameQuestion = logoList.get(position);
            UserQuestion userQuestion = userQuestionMap.get(gameQuestion.questionId);
            if(convertView == null) {
                int layout_id = R.layout.row_logo_new;
                LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(layout_id,null);
            }

            TextView txtLogoHeader = (TextView) convertView.findViewById(R.id.txtLogoQuestion);
            //TextView txtLogoDetails = (TextView) convertView.findViewById(R.id.txtLogoDetails);
            TextView txtLogoPoints = (TextView) convertView.findViewById(R.id.txtLogoPoints);
            TextView txtEarnedCredit = (TextView) convertView.findViewById(R.id.txtEarnedCredit);
            TextView txtHintsUsed = (TextView) convertView.findViewById(R.id.txtHintsUsed);

            LogoStar ls = (LogoStar) convertView.findViewById(R.id.customLogoStar);
            ls.setRating(gameQuestion.questionComplexity);
            txtLogoHeader.setText(gameQuestion.questionText);
            txtLogoPoints.setText("Total Credits : " + Integer.toString(SGUtil.getCredits(gameQuestion.questionComplexity)));

            if(userQuestion.isAnsweredCorrectly == 1 ) {
                txtEarnedCredit.setText("Earned Credits : " + Integer.toString(SGUtil.getScore(gameQuestion.questionComplexity, userQuestion.currentHintId)));
            } else {
                txtEarnedCredit.setText("Earned Credits : " + 0);
            }

            txtHintsUsed.setText(Integer.toString(userQuestion.currentHintId) + " Hints used");
            return convertView;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo_selector);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.game_logo_toolbar);
        setSupportActionBar(myToolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        int gameId = getIntent().getIntExtra("gameId",0);

        if(gameId == 0){
            gameId = Preferences.readInteger(getApplicationContext(), "gameId", 0);
            if(gameId != 0 ) {
                Preferences.writeInteger(getApplicationContext(), "gameId", 0);
            }
        }

        if(gameId != 0) {
            DatabaseHelper.openDb();
            GameQuestionDao gameQuestionDao = new GameQuestionDao();
            List<GameQuestion> gameQuestions = gameQuestionDao.getGameQuestions(gameId);
            UserQuestionDao userQuestionDao = new UserQuestionDao();
            List<UserQuestion> userQuestions = userQuestionDao.getUserQuestions(gameId);
            DatabaseHelper.closeDb();
            userQuestionMap = getUserQuestionMap(userQuestions);
            logoList.addAll(gameQuestions);
            final ListView lv = (ListView) findViewById(R.id.ListLogo);
            ListAdapter adapter = new ListAdapter(LogoSelectorActivity.this);
            lv.setAdapter(adapter);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    GameQuestion gameQuestion = logoList.get(position);
                    Intent intent = new Intent(LogoSelectorActivity.this,QuizActivity.class);
                    intent.putExtra("gameId",gameQuestion.gameId);
                    intent.putExtra("questionId",gameQuestion.questionId);
                    startActivity(intent);
                }
            });
        }
    }

    private Map<Integer, UserQuestion> getUserQuestionMap(List<UserQuestion> userQuestions) {
        Map<Integer, UserQuestion> userQuestionMap = new HashMap<>();
        if(userQuestions != null) {
            for(UserQuestion userQuestion : userQuestions) {
                userQuestionMap.put(userQuestion.questionId,userQuestion);
            }
        }
        return userQuestionMap;
    }
}
